\chapter{Stochastic Simulations}
In this chapter, we present the main aspects of stochastic simulations so that the reader will be able to understand in detail the simulator we built and that will be shown later.
A stochastic simulation \cite{perf, sim} is nothing more than a computer experiment where the real environment is replaced with a program execution; in this experiment variables change stochastically with individual probabilities. Simulations are a tool that helps us to design or study complex systems. Attention must be paid in modelling the system randomness, in the statistical analysis of the results and also in the validation of the simulation itself. This tool offers both advantages and drawbacks.\\ Drawbacks reside in the fact that, usually, simulations can be expensive and time-consuming to develop, moreover for each run of the simulation model we only obtain estimates of a model's real characteristics consequently different independent runs will be needed. Furthermore,  if we do not verify carefully that the simulation is a fair reproduction of the real system  or of a model the obtained results are completely useless.\\
Nevertheless, they also offer several advantages such as the possibility of approaching also complex,  real-world systems that we would not be able to approach accurately with mathematical models, the fact that simulations allow us to estimate performance indexes of an existing system under different experimental circumstances, over which  we have clearly more control than in the case we were dealing with the real system. Finally, notice that simulations are also often used to compare different systems designs.\\


One  of the first things we need to understand when talking  about simulations is the difference between \textit{real} time and \textit{simulated} time. In a simulation the flow of time is controlled by a computer: it will be necessary to deal with a certain parallelism as in real systems more actions can happen at the exact same time whereas in the simulated program they will be serialized. This is done taking into account  the simulated time, i.e. the time in which the event should happen in the real system. Every action is then decomposed into instantaneous events (for example, the arrival of a customer to the system) e and we assume that two events cannot happen at the exact same time.\\
The real time of a simulation depends instead on the performance of the computer we are using to execute the simulation (e.g. speed of the processor, amount of memory available) and on the level of optimization of the simulation program.
We now need to define some basic concepts:
\begin{itemize}
\item[\adforn{43}] A \textit{system} is a collection of components that interact among them in order to satisfy some requirements. At every moment in time, the system is in a particular situation that we call \textit{state}; this is determined by the instantaneous conditions of all the system components. Notice that a system evolving in time is described from the state history. 
\item[\adforn{43}] A \textit{model} of a system is its mathematical representation, as a matter  of fact,  the model of a real system offers a certain abstraction level. It is important to remember that the model by itself does not represent the history of the system as a matter of fact  a model state is always associated with a state of the real system.
\item[\adforn{43}] A model presents different types of variables.
\begin{itemize}
\item[\adforn{14}] \textit{Exogenous} variables depend on the environment, if we can control them we call them \textit{parameters}.
\item[\adforn{14}] \textit{Endogenous} variables depend on the model itself, in practice they are the output results.
\item[\adforn{14}] \textit{State} variables describe the state of the system in a certain moment and they vary in time interacting with endogenous and exogenous variables; remember that these interactions are defined by the characteristics of operations.
\end{itemize}
\end{itemize}
To simulate a model means to produce the history of the states of the model and then interpret  it as the history of the states of the system. The simulated model is said  to be the \textit{simulation model} whereas the simulation on the computer is called \textit{digital simulation}. The suitability of a system model is decided according to the study goals.

\section{Types of Simulation}
There are different types of simulation, usually the following classification is used.

\paragraph*{Deterministic/Stochastic.} A deterministic simulation does not have random components, this type of simulation is used when we want to test some property of a system for which the environment is completely known, for example to test the robustness of a certain implementation. In the majority of cases however, this type of simulation is not sufficient nor available (in fact, it is not easy to know everything about a system) as a matter of fact random components help us to better model the system environment; a direct consequence of stochastic simulations is that also the output will be random and this will allow us to gather statistics and perform data analysis.

\paragraph*{Terminating/Non-terminating.} A simulation is said to be \textit{terminating} if it terminates when a particular condition happens; for example, if we want to evaluate the execution time of a sequence of operations in a certain well-defined environment, it is possible to execute the sequence with the simulator and keep record of the simulated time. Typically, we use terminating simulations when we  are interested in the lifetime of a  certain system or when the system inputs depend on time.

\paragraph*{Asymptotically stationary/Non-stationary.} The stationary condition can be verified only in case of non-terminating stochastic simulations as stationarity is a property that needs to be evaluated over time. Often, the simulation state depends on the initial conditions and it may be difficult to find favorable initial conditions; for example, if we simulate an information server probably we would start with an empty buffer but this hypothesis is actually too optimistic because in a real system executed for a certain amount of time there will be data structures that will not be empty.\\
Stationarity is a solution to this problem, as a matter of fact a stationary simulation, i.e. that has a unique stationary regime, is such that we do not retrieve any useful information  from its initial state as its state distribution becomes independent of the initial conditions.\\
Unfortunately, in practice a simulation is rarely exactly stationary however it can be asymptotically stationary; meaning that after some simulated time, the simulation will get to a stationary state. More precisely, we have that a simulation with time independent input can always be thought of as a Markov chain. Remember that a Markov chain is a generic stochastic process such that to simulate the future after a certain time $t$, the only information needed is the state of the system at time $t$ (\textit{memoryless property}). Markov chain theory states that the simulation can converge or diverge to a stationary behaviour. If our aim is to measure some performance indexes of the system under study we are probably interested in its stationary behaviour. Different reasons could cause the absence of an asymptotically stationary regime, for example:
\begin{itemize}
\item[\adforn{43}] Unstable models: in queueing systems where the arrival rate is greater than the service capacity, in this case the longer the  execution time of the simulation, the longer will be the length of the queue, i.e. it will grow indefinitely without reaching a stationary state,
\item[\adforn{43}] \textit{Freezing simulation}: the simulated system does not converge to a stationary regime and instead freezes becoming slower and slower. This behaviour is usually caused by some rare event that drastically alters the system's functioning. The probability of observing a rare event is proportional to the length of the simulation time. If, by chance, the simulation has some regenerative state (i.e. a point in which the system reaches a clean state, e.g. if it becomes empty again) then the simulation freezes if the interval of time between two regenerative points has an infinite average.
\item[\adforn{43}] Models with inputs depending on the simulation time, in this type of models is exactly this dependence to prevent the system from the achievement of a stationary regime; an example of this situation may be a the internet traffic that grows every month and that in some moments of the day is more intense.
\end{itemize}

Notice, in particular, that even if the initial state does not influence the stationary behaviour it does influence the time required to reach the stationary regime. Moreover, it is important to underline that in stationarity the state of the model does not  become constant but it is the probability of observing a certain state that stabilizes.

In the majority of cases when we execute a non-terminating simulation it is necessary to be sure that this reaches a stationary regime; otherwise, the simulation output and the consequent analysis will depend on the initial condition and on the length of the simulation itself.

\subsection{Simulation Techniques}
Here, we present the three main classes of simulators.

\paragraph*{Discrete Event Simulation - DES.} In this case, we model a set of entities which state evolves in time, these entities interact in order to compete for resources or to synchronize on some particular events. The crucial point in this type of simulation is the maintenance of the list of events that need to be processed. In the next section, we will describe in detail this type of simulation as it is the one that we used for our project.

\paragraph*{System Dynamics - SD.} With respect to the previous type, the approach is top-down as a matter of fact we model the entire system instead of the singular entities; to model the connection among state variables, differential equations are used. This technique is used when the  system has a big number of identical or very similar entities and consequently it is more appropriate to consider their multiplicity rather than their identity.

\paragraph*{Agent Based Simulation - ABS.} This technique consists of simulating the decisional process of every single agent that is part of the system, the final goal usually is to observe the behaviour of the system emerging as the result of agents interaction. 

\section{Random Number Generator}

A simulation of a system in which we recognize random components clearly requires some method of generating numbers that are random. We know computers are deterministic machines, exception made for some particular kind of hardware that is able to generate only numbers that are actually pseudo-random and not completely random. It is possible to limit the problem to the generation of random numbers in the interval $(0,1)$ with uniform distribution, from which then other distribution are generated. Random number generators (RNG) must have the following properties:
\begin{itemize}
\item[\adforn{43}] the generated number must belong to the interval $(0,1)$, they must be uniformly distributed and there shall be no correlation among the various extractions
\item[\adforn{43}] they should be fast and they should not occupy too much memory
\item[\adforn{43}] extractions should be reproducible
\item[\adforn{43}] different streams of random numbers should  be available
\item[\adforn{43}] the random number generator should be portable
\end{itemize}

The generation of (pseudo)random numbers is a very delicate topic as a lot of generators available in software or libraries are not adequate to be used in simulations. Now, we give a formal definition \cite{rng2} of (pseudo)random number generator and then we will present the one we implicitly used in our simulator i.e. the \textit{Marsenne Twister} generator \cite{rng}.

\begin{definition}[Random Number Generator]
Mathematically, a (pseudo)random number generator  can be defined as a quintuple $(S, \mu, f, U, g)$,  where $S$ is a finite set of  states, $\mu$ is a probability  distribution on $S$ used to select the initial state $s_0$, called \textit{seed}, $f: S\to S$ is the transition function, $U=[0,1]$ is the output set and finally $g:  S\to U$ is the output function.
\end{definition}
The state evolves according to the following recurrence $$s_i=f(s_{i-1})\qquad \mathrm{for}\ i\geq 1$$
and the output of step $i$ is:
$$u_i=g(s_i)\in U$$
these $u_i$ are the so called pseudo(random) number produced by the generator. Since set $S$ is finite sooner or later the generator will come back to a previously visited state, i.e. $$s_{l+j}=s_l\quad \mathrm{for\ some}\ l\geq 0\ \mathrm{and}\ j>0$$
consequently $$s_{i+j}=s_i\ \mathrm{and}\ u_{i+j}=u_i\ \forall i\geq l.$$
The smallest  value of $j>0$ for which this happens is called \textit{the length of the period}, denoted by the Greek letter $\rho$. Clearly, this value cannot be greater than $S$, in particular if $b$ bits are used to represent a state then we will have $\rho\leq 2^b$.
The best (pseudo)random number generators are built in such  a way that the length of their period gets as close as possible to that limit.

\subsection{Marsenne-Twister Random Number Generator}
Marsenne-Twister is a (pseudo)random number generator,  is the \textit{general-purpose} generator most used. It was developed in 1997 by Makoto Matsumoto  and Takuji Nishimura. The algorithm of this generator generates an optimal set of (pseudo)random numbers, this is because the creators tried to fix the known issues of the generators existing at the time when they were creating their own generator. \\
The name of the generator derives from the fact that the length of its period is chosen so that it is a Marsenne prime number. In mathematics, a Marsenne prime number satisfies the following property $$M_p=2^p-1$$ with $p$ that is a positive prime integer.\\
Among the many advantages that this generator offers we recall the following:
\begin{itemize}
\item[\adforn{43}] it has a huge period that equals $2^{19937}-1$ and this was proved by the creators of the algorithm
\item[\adforn{43}] it passed different randomness statistical tests
\item[\adforn{43}] it turned out to be more efficient than other algorithms that do not even get close to it as far as quality is concerned
\item[\adforn{43}] it is extremely portable, as a matter of fact it is included in different programming languages and libraries.
\end{itemize}

\section{Discrete-Event Simulation}
The majority of communications systems are simulated using discrete-event simulations. To deeply comprehend this technique we must keep in mind the concepts of entity, i.e. the elements that interact, and of event, intended as the  change of state of one or more entities. The most important  aspects of this methodology reside in keeping track of a global variable that will have the name \texttt{currentTime} and in the presence of an event scheduler.\\
Events are objects that represent different transitions, every event has its associated time (i.e. \texttt{timestamp}); an event scheduler is nothing more than an ordered list organized in ascending order according to \tt{timestamp}. What happens is that the simulation program takes the first event from this list, moves \tt{currentTime} to the \tt{timestamp} of the picked event and executes the event. The execution of this event may cause the  scheduling of new events with a grater \tt{timestamp} with respect to \tt{currentTime} and it may also cause the order change or the cancellation of some events that  were already in the event list. Remember that the global variable \tt{currentTime} should never be modified  by an event except for the fact that as events progressively take place \tt{currentTime} jumps from a \tt{timestamp} value to another. This is exactly why it is called discrete event simulation as the events that change the system states form a discrete set. Notice also that event not only gave to simulate the logic of the system but they also have to keep updated some counters that will be needed in order to perform output data analysis.\\

In practice, a simulator event oriented is realised as follows; we maintain a calendar of events that we call \textit{Future Event List} (FEL), from which the event with the smaller \tt{timestamp} is extracted. A subroutine manages  the event  modifying the state of all the entities influenced by the event and eventually modifying the value of the endogenous variables (output), the same subroutine also updates the calendar of events. We  show two diagrams in fig. \ref{fig11} that present the functioning of discrete event simulations intuitively.

As far as the FEL management is concerned some basic operations are required such as: event insertion, extraction of the next event to be processed, deleting of an  arbitrary event (i.e. not necessarily the event with the smaller \tt{timestamp}). The advancement of the simulated time, as we have seen proceeds jumping from one timestamp event to another. This advancement can proceed
\begin{itemize}
\item[\adforn{43}] with fixed intervals, meaning that time advances of $\Delta t$ at each step and all the events happening in that interval of time must be simulated (these events will be considered simultaneous), with this option some problems should be taken care of such as how to treat the multiple events and how to choose $\Delta t$
\item[\adforn{43}] with the events i.e. the time advances of a quantity equal to the time needed for the next event in the FEL to happen.
\end{itemize} 

\begin{figure}[H]
	\centering
	\includegraphics[width=12cm]{Images/img11.png}
	\caption{On the left the fundamental cycle of a discrete event simulation and on the right the processing of an event.}
	\label{fig11}
\end{figure}


\section{Output Analysis}
When dealing with simulation output it is important to  analyse it appropriately. This is mainly because each run of a simulation
offers us a possible realization of the random variables  underlying the simulation. Considered this, it is easy to understand why one run of arbitrary length of the simulation program is not enough to consider the retrieved results as the  real characteristics of  the system under study. In general, we produce $n$ independent runs, where independent means that for each run we  change the seed for the generation of the needed random numbers. Notice, that the initial settings and the configuration of the simulation are identical for all the independent runs.
Furthermore, as initial conditions usually do not represent fairly the conditions we would find the system in, in an arbitrary moment we consider, for the gathering of  the statistics of interest, the steady-state behaviour of the stochastic process underlying the simulation. In fact, we do not want our results to be biased towards the initial settings of the simulation.

\subsection{Validation}
The main goal of validation is to produce a simulation model that represents accurately enough the real system behaviour so that the simulation can be used to experiment the system under different circumstances, to analyse its behaviour and predict its performance. Validation is defined in \cite{val} as determining whether the model's output behaviour has a satisfactory range of accuracy for the model's intended purpose over the domain of the model's intended applicability. 


The level of accuracy required is usually defined by the range within which the difference between a model's output variable and the corresponding system output variable must be contained. This range is commonly known as the model \textit{acceptable range of accuracy}. When the variables of interest are random variables, these are usually of primary interest and they are usually the quantities used to determine the model validity.

To obtain a high degree of confidence in a simulation model and its results, comparisons of the model's and system's output behaviours are usually required. However, often the situation is such that the system is not observable consequently  it is usually not possible to obtain a high degree of confidence in the model. In this situation, the usual approach is to carefully analyse the model output behaviour and to make some comparisons with other valid models (also theoretical ones) whenever possible.

One of the most known approaches used in comparing the simulation model output behaviour to either the system output behaviour or another (validated) model output behaviour is the use of confidence intervals as they allow us to make an objective decision. Notice that this approach is not always available as it is not frequent that  the statistical assumptions required can be satisfied or in any case it is not an easy approach to use as sometimes we satisfy these assumptions only with great difficulty (required assumptions are usually data independence and normality). Another issue may be a highly non-stationary  behaviour or also the quantity of data available, as a matter of  fact it may be expensive and not efficient at all to retrieve the desired dataset. Nevertheless, confidence intervals can be obtained for the differences between means, variances, and distributions of different output variables of a simulation model and a system for each set of experimental conditions. These intervals can be used as the model range of accuracy for model validation, where the model range of accuracy is the confidence interval or region around the estimated difference between some function (e.g., the mean) of the model and system output variable being evaluated.

\subsection{Warm-up Period and Independent Replications}
As far as non-terminating simulations are concerned it is necessary to distinguish two phases, a transient phase (initial phase or \textit{warm-up} phase) and a stationary phase that is generally reached, as explained above, for $t\to\infty$ where $t$ represents the simulated time.\\
Statistics gathered during the transient phase depend on the initial state therefore they are of no interest for the statistical analysis of the obtained results. One of the main issues of non-terminating simulations is exactly  when to end them, this is because in practice we cannot rely  on the stationarity condition $t\to\infty$ and it is necessary to understand when the stationary regime was reached because it is there that we will collect the  statistics of interest.
In order to identify the warm-up period and consequently the beginning of the stationary regime i.e. the point from which we will start to gather data, we can use  Welch's graphical method. This method is presented below.

\paragraph*{Welch's Graphical Method.} Suppose we want to estimate the mean of a sequence of observations to be intended as random variables $Y_1, Y_2, Y_3, \dots$ in the stationary regime, in practice we want to estimate
$$\nu=\lim_{i\to\infty} \E[Y_i]$$
consequently, we will have a finite sequence of observations $Y_i$ with $i\in[1,m]$ (where $m$ is the number of gathered observations) and we will look for a value $l$ that allow us to define:
$$\bar{Y}_l=\dfrac{\sum_{i=l}^m Y_i}{m-l}$$
where $\bar{Y}_l$ is the average of the random variables of observations from $l+1$ to $m$, the  hope is to obtain $\nu\sim\bar{Y}_l$. Notice that  it is important to choose $l$ accurately because if it is too small then $\bar{Y}_l$ will be influenced by the initial observations that are not stationary, whereas if it is too big (so maybe near to $m$) the number of random variables used to compute $\bar{Y}_l$ would be too small and the accuracy of $\nu$ estimation would be too low. To choose $l$ we do what follows.

We process $n$ pilot executions (with $n$ possibly greater or equal to 5), each execution will gather $m$ observations, so that $Y_{ji}$ will be the random variable associated with the $i$-th observation of execution $j$ with $1\leq i\leq m$ and $1\leq j\leq n$. Now, we define the sequence made up of the averages, 
$$\bar{Y}_i=\dfrac{\sum_{j=1}^n Y_{ji}}{n}$$
this will reduce the variance of the process. At this point, we create  a window of width $w\leq \lfloor m/4\rfloor$ and we construct a sequence of $m-w$ random variables defined as the averages of the $w$ previous $\bar{Y}_i$, the current one and the $w$ following $\bar{Y}_i$:
$$\bar{Y}_i'=\dfrac{\sum_{s=i-w}^{i+w} \bar{Y}_s}{2w+1}$$
Notice that for random variables from $1$ to $w$ the average is computed as
$$\bar{Y}_i'=\dfrac{\sum_{s=1}^{2i-1} \bar{Y}_s}{2i-1}$$
In practice we are computing a moving average on the process composed by the random variables given by the mean of a same observation over different runs. Finally, we draw a graph of the $\bar{Y}_i'$ observations for $i=1,\dots, m-w$ and  we choose a value for $l$, beyond which the series of $\bar{Y}_i$ seems  to be more stable.\\

As anticipated before we know that non-terminating simulations do not have a natural stopping point. However, it is necessary to decide when to interrupt the simulation and with it the collection of statistics. Intuitively, what would be ideal is  to interrupt  the  simulation when the quality of the obtained estimates is reasonably good. In practice, the basic idea to evaluate the quality of the  estimates consists of executing independent runs and then compare their estimates. The empirical rule states to make the duration of a simulation lasts the time sufficient to do so that the number of repeated execution give estimates that together approximately represent a random Gaussian variable. But it is necessary to pay attention not to extend excessively a simulation as this might provoke inconsistency in the obtained results because of time and resources consumption, of numeric instability of floating-point and also because of the periodicity of the random number generators.

Summarizing, to avoid initialization bias we use Welch's graphical method, then in order to produce estimates of quantities of interest and their respective confidence intervals we can use the method of independent replications.  Basically, it consists of computing the average over the runs of the observations for each quantity of interest, excluded the ones cut off with  Welch's graphical method,  and then we use the results to  construct confidence intervals.




