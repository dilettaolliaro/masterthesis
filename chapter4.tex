\chapter{Deriving Product-Form Solutions}
As we have already anticipated in the previous chapters, when studying a queueing system, we are particularly interested in its product-form expression, if one exists. Unfortunately, product-form solutions often lead to complex and time-consuming computations. In fact, historically, in order to retrieve a product-form expression one typically had to guess that such an expression existed and then verify that the Kolmogorov's equations of the underlying Markov process were satisfied. This task becomes more and more difficult to tackle as the complexity of the system grows. For this exact reason, researchers have been trying to apply different methods to derive the same result.\\
The approach we are going to present in this chapter is the one developed by P.G. Harrison in \cite{rcat}; the approach is based on the so called \textit{Reversed Compound Agent Theorem} (RCAT). First, we are going to present what is a Stochastic Process Algebra and in particular, we are going to present the main features of PEPA (\textit{Performance Evaluation Process Algebra}). Secondly, we are going to show RCAT itself and one of its extensions that were useful to our aim and finally we will propose an example of use.

\section{Performance Evaluation Process Algebra}
PEPA is a stochastic process algebra. Process algebras \cite{pepa2} emerged firstly as a modelling technique for the  functional analysis of concurrent systems. Stochastic process algebra was then proposed as a tool for performance and dependability analysis and modelling. The main attractive feature of SPA was the fact that compositionality was explicit. Compositionality means that a complex expression is actually  determined by the meanings of its constituent expressions and the rules used to combine them, in practice it is the ability to  model a system as the interaction of its subsystems. In fact, this principle was applied to systems and this resulted particularly useful because when a system is composed of interacting components we are now able to model interactions and components separately.\\

PEPA is a stochastic process algebra designed for modelling computer and communication systems by J. Hillston as presented in \cite{pepa}. This formalism can be used for studying quantitative and qualitative properties of the model under analysis. In particular, it provides an elegant syntax for expressing continuous-time Markov processes in  a compositional way. In fact, PEPA was the first language to be developed with the explicit intention of generating Markov processes that could be solved numerically for performance evaluation. PEPA peculiarity, with respect to other stochastic process algebras, consists in the fact that it associates a random variable, that will represent duration, with every action. These random variables are assumed to be exponentially distributed and this is the connection between this process algebra and Markov processes.

\subsection{Syntax}

PEPA is based on three main ingredients: \textit{components} that are the active units within the system, \textit{activities} that capture the actions of those units and \textit{cooperation} that expresses the interaction between components.

Models are constructed from components. These components perform activities and, as we have already said, each activity has an associated duration described by an exponentially distributed random variable. Moreover, each activity has an \textit{action type} $\alpha$ and an \textit{activity rate} $r$. Each system action is uniquely typed and there is a countable set $\mathcal{A}$ of all possible types, activities with the same action type are different instances of the same action by the system. 
Since an exponential distribution is uniquely determined by its  parameter, the duration of an activity is represented by a single real number parameter, the so-called \textit{activity rate}. This rate may assume the value of any positive real number of the distinguished symbol $\top$ that has to be read as \textit{unspecified}. We now present some notations, also summarizing the concepts presented so far.
\begin{itemize}
\item[\adforn{14}] $\mathcal{A}$ is the set of all action types ($\tau$ included, that denotes the type \textit{unknown})
\item[\adforn{14}] $\mathcal{R}^+$ is the set of all positive real numbers, including $\top$
\item[\adforn{14}] $\mathcal{A}ct=\mathcal{A}\times \mathcal{R}^+$ is the set of  all activities
\end{itemize}
Therefore, an activity is denoted as $$a=(\alpha, r)$$ with $a\in\mathcal{A}ct, \alpha\in\mathcal{A}$ and $r\in\mathcal{R}^+$. Typically, components are denoted by upper-case letters, activities by lower-case letters and action types by Greek letters.
The combinators of the language allow expressions, or terms, to be constructed defining the behaviour of the components via  the  activities they  undertake and the interactions between them. The behaviour of each component and consequently of the whole system is described with the following semantic.
\begin{itemize}
\item[\adforn{43}] \textit{Prefix} $$C = (\alpha, r).S$$ This means that component $C$ carries out activity $(\alpha, r)$ and then it subsequently  behaves  as  component $S$. A delay is thus inherent in each activity in the model and the timing behaviour of the system is captured, moreover since the duration is a random variable also the uncertainty on how long an action will take is represented.

\item[\adforn{43}] \textit{Choice} $$C = S_1+S_2$$ This means that component $C$ represent a system which may either behave as component $S_1$ or as component $S_2$. In any case, $C$ enables all the current activities both of $S_1$ and of $S_2$ but only the activity that completes first will decide the behaviour of $C$ and the other component of the choice will be discarded. Two facts need to be highlighted: the first one is that the continuous nature of the probability distributions ensures  that the probability that $S_1$ and $S_2$ both complete an activity at the exact same time is 0; the second one is that the \textit{choice} combinator implicitly assume that $S_1$ and $S_2$ are competing for the same (implicit) resource therefore, the combinator $+$ represent the competition between components.

\item[\adforn{43}] \textit{Cooperation} $$C = C_1\bowtie C_2$$ This is an \textit{indexed family of combinators}, one for each possible set of action types $L\subseteq \mathcal{A}$. $L$ is the so-called \textit{cooperation set} and defines the action types on which the components involved must synchronize. $\bowtie$ assumes that each component proceeds independently with any activities whose types do not occur in $L$. Whereas, activities with action types in $L$ require the simultaneous involvement of both components in an activity of that type (notice that $\tau\notin L$); accordingly one component may become blocked waiting for the other component to be ready to participate.
The cooperation, in practice, forms a new shared activity with the same action type as the two cooperating activities and with rate reflecting  the rate of the \textit{slower} participant. If one of the two cooperating activities has an unspecified rate in a component, then the component is said to be passive with respect to that action type.

\item[\adforn{43}] \textit{Parallel Composition} $$C= C_1 || C_2$$ This represents exactly cooperation when $L=\emptyset$ which means that components are allowed  to proceed  concurrently without interactions between them. It exactly as if we wrote $C_1 \underset{\emptyset}\bowtie C_2$.

\item[\adforn{43}] \textit{Assignment} $$A \overset{def}= P $$This means that a \textit{constant} component $A$ behaves as component $P$.

\end{itemize}

A race condition governs the dynamic behaviour of a model whenever more than one activity is enabled, this means that  when many activities try  to  proceed only the fastest will succeed. Consequently, the probability  that a particular activity completes will be given by the ratio of the activity rate of that activity to the sum of the activity rates of all the enabled activities.\\

As already anticipated, a component may be passive with respect to an action type; this means that all the activities of that type enabled by the component will have an unspecified rate $\top$. These activities must be shared with another component so that the other component can determine the rate of  this  shared activity. If more than one activity of a given passive type can be simultaneously enabled  by a component, each unspecified activity rate must be assigned a  weight; weights are natural numbers used to determine the relative probabilities of the possible outcomes of the activities of that action type. If no weights are assigned  we assume that multiple instances have equal probabilities of occurring.\\

When we have $P=(\alpha, r).S$ we say $S$ is a  derivative of $P$ if, as in this case, there only one activity between the two then we say that $S$ is a \textit{one-step derivative} of $P$. The \textit{derivative set} of a PEPA component $P$, denoted $ds(P)$ is defined as the smallest set of components such that if $P\overset{def}=P_0$ then  $P_0\in ds(P)$ and if $P_i\in  ds(P)\wedge \exists\ a\in\mathcal{A}ct(P_i)$ such that $P_i = a.P_j$ then $P_j\in ds(P)$. In practice, the derivative set of a component $P$ is  the set of all PEPA components representing all the reachable states of the system  by $P$.

Given a PEPA component and its derivative set, its derivation graph is the labelled directed multigraph whose set of nodes is the derivative set  of the component and whose set of arcs is defined by the activities connecting the states of the graph.

\section{Reversed Compound Agent Theorem}

The reversed compound agent theorem enables us, through the use of PEPA, to model and interpret in a simpler way the interactions and synchronization actions among different  components of a system model. Using this approach, we are able to retrieve product-form solutions for the system under study without the need of solving the, computationally heavy, global balance equations. In this section we are going to present the first version of this theorem omitting the details of the proof, then we are going to show some of the extensions developed for this approach and finally we are going to show an operational example in which we try to obtain the same product-form for a simple queueing system using Jackson's theorem and RCAT.\\

RCAT exploits an abbreviated PEPA syntax in which we consider the prefix combinator, the cooperation between two agents (components) and the constant agent defined by the assignment combinator.  As we have seen in standard PEPA the shared actions of a cooperation occur at the rate of the slowest component; nevertheless, now, we require that for each action type in the cooperation set, exactly one agent is \textit{passive} and concretely  its synchronising action has rate $\top=\infty$. This, basically means that the passive agent actually waits for the other one.  The set of actions, which an agent $P$ may next engage in, is called the set of  \textit{current actions} and when the system is behaving as agent $P$ these are the actions that are \textit{enabled}. In addition, the derivation graph, formed by syntactic PEPA terms at the nodes, with arcs representing the transition between them, determines the underlying Markov process  of an agent $P$. The transition rate between two agents, $C_i$ and $C_j$, denoted $q(C_i, C_j)$, is the sum of the action rates labelling arcs connecting $C_i$ and $C_j$.

Here, we also need to introduce relabelling, which preserves the semantic but will be useful to define the reversed process of cooperations: $P\{x\leftarrow y\}$. describes agent $P$ in which all occurrences of symbol $y$ have been replaced by $x$; $y$ may be an action type or also a rate. 
Notice that from now on we will refer to all agents as simple if they are  defined only through prefixes and assignments, whereas we will say an agent is compound if it contains at least one instance of the cooperation combinator.

\subsection{Rates of the Reversed Actions}

It is rather simple to find a PEPA agent definition $\bar{X}$ that has the derivation graph with arrows in the opposite direction with respect to those of a given  agent $X$. What it is not trivial is to find the appropriate rates of the reversed actions to make $\bar{X}$ the actual reversed process of $X$ as defined in section \ref{sec14}.

For simple agents, we can directly analyse the state transition graph of the Markov process and after determining the reversed graph $\bar{G}$, we proceed as follows:
\begin{enumerate}
\item We use the conservation of outgoing rate to equate $q_i^R=q_i$ for all states $i\in\mathcal{S}$
\item We find a covering set of cycles, we may choose the set of all cycles or the set of all minimal cycles in $G$. If $\mathcal{S}$ is infinite then the set of cycles is also infinite but in practice cycles often repeat in a parametrised way so that the number of cycles to be considered will be finite.
\item For each (parametrised) cycle, we apply Kolmogorov's criteria i.e. we equate the known product of the  rates around the cycle in $G$ with the symbolic product of the rates around the reversed cycle in $\bar{G}$. Some of the reversed rates may be already known or imposed by the previous steps or from previously considered cycles in this step, which simplifies the resulting equations.
\end{enumerate}

The result will be a system of possibly non-linear equations that uniquely determines the reversed rates in $\bar{G}$. This must be so because of the necessity and sufficiency of Kolmogorov's criteria and the uniqueness of the equilibrium state probabilities in an ergodic Markov chain. Notice that one of the reasons for analysing simple agents is to provide base cases for a compositional analysis of larger Markov chains. In fact, any continuous Markov chain can be described using only simple agents, however, the fact that an agent can perform multiple actions leading to the same derivative causes multiple arcs in the derivation graph and consequently also between two states in the transition graph of the underlying Markov chain. This does not create any issue as we can always determine the total reversed rate between any two states with multiple arcs between them, using one of the known methods. However, we need to consider multiple actions individually in cooperations. This is because an agent may have several actions leading to the same derivative that synchronise with different actions in a cooperating component. For example, this can happen when a queue witnesses a departure of a job, this job may join other queues or also leave the system.

In the reversed cooperation, the portion of the total reversed rate allocated to  each individual reversed arc is crucial therefore a rule is needed. The rule we use is the following:
\begin{definition}
The reversed actions of multiple actions $(a_i, \lambda_i)$ for $1\leq i \leq n$ that an agent $P$ can perform, which lead to the same derivative $Q$, are respectively
$$\bigg(\bar{a}_i, \bigg(\dfrac{\lambda_i}{\lambda}\bigg)\bar{\lambda}\bigg)$$
where $\lambda=\lambda_1+\dots+\lambda_n$ and $\bar{\lambda}$ is the reversed rate of the one-step, composite transition with rate $\lambda$ in the Markov chain, corresponding to all the arcs between $P$ and $Q$.
\end{definition}

In other words, the total reversed rate is distributed amongst the reversed arcs in proportion to the forward transition rates.

\subsection{Compound Agents}
Under appropriate conditions, the reversed agent of a cooperation between two agents $P$ and $Q$ is a cooperation between the reversed agents of $P$ and $Q$, after some reparametrisation. Before formally showing this result (\ref{th3}), we first need to define some new notation.

\begin{definition}
The subset of actions types in a set $L$ which are \textit{passive} with respect to a process $P$ (i.e. are of the form $(a, \top)$ in $P$) is denoted by $\mathcal{P}_P(L)$. The set of the corresponding active action types is denoted by $\mathcal{A}_P(L)=L\setminus \mathcal{P}_P(L)$.
\end{definition} 

Last thing to do before considering the following theorem we need to syntactically transform the agent under analysis so that every occurrence of a passive action $(a, \top)$ is relabelled as $(a, \top_a)$; this guarantees that every passive action rate is uniquely identified with exactly one action type.

\begin{theorem}[Reversed Compound Agent Theorem]\label{th3}
Suppose that the cooperation $P\underset{L}\bowtie Q$ has a derivation graph with an irreducible subgraph $G$. Given that
\begin{enumerate}
\item every passive action type in $\mathcal{P}_P(L)$ or $\mathcal{P}_Q(L)$ is always enabled in $P$ or $Q$ respectively (i.e. enabled in all states of the  transition graph);
\item every reversed action of an active action type in $\mathcal{A}_P(L)$ or $\mathcal{A}_Q(L)$ is always enabled in $\bar{P}$ or $\bar{Q}$, respectively;
\item every occurrence of a reversed action of an active action type in $\mathcal{A}_P(L)$ (respectively, $\mathcal{A}_Q(L)$) has the same rate in $\bar{P}$ (respectively, $\bar{Q}$)
\end{enumerate}
then the reversed agent $\overline{P\underset{L}\bowtie Q}$ with derivation graph containing the reversed subgraph $\bar{G}$, is 
$$\bar{R}\{(\bar{a}, \overline{p_a})\leftarrow(\bar{a}, \top)|a\in\mathcal{A}_P(L)\}\underset{L}\bowtie \bar{S}\{(\bar{a}, \overline{q_a})\leftarrow(\bar{a}, \top) | a\in\mathcal{A}_Q(L)\},$$
where
\begin{align*}
&R=P\{\top_a\leftarrow x_a| a\in\mathcal{P}_P(L)\},\\
&S=Q\{\top_a\leftarrow x_a | a\in\mathcal{P}_Q(L)\},
\end{align*}

$\{x_a\}$ are the solutions (for $\{\top_a\}$) of the equations

\begin{align*}
&\top_a = \overline{q_a},\quad a \in\mathcal{P}_P(L)\\
&\top_a = \overline{p_a},\quad a \in\mathcal{P}_Q(L)
\end{align*}

and $\overline{p_a}$ (respectively, $\overline{q_a}$) is the symbolic rate of action type $\bar{a}$ in $\bar{P}$ (respectively $\bar{Q}$)

\end{theorem}

As far as conditions 1. and 2. are concerned it is rather straightforward to check if the passive actions are enabled in every state of the two agents $P$ and $Q$ and if the reversed active actions are also always enabled. Condition 3. may require a little more work if the rates of the reversed processes are not known. Nevertheless, we can assume that condition 3. holds and solve the equations to find the reversed rates (which will be precisely the traffic equations for the internal flow butt we will refer to them as \textit{rate equations} because we may  deal with more general models than queueing networks) and if these equations have a  solution, we see that the condition is satisfied. The existence of a rate equations solution ensures the product-form of the model which follows naturally once the reversed process and its rates have been defined.

\paragraph*{Propagation of Instantaneous Transitions.} In \cite{rcat2} the authors have proved that RCAT can be applied also to models that involve propagating synchronizations. To understand what we mean we briefly need to discuss the concept of G-network. G-networks \cite{gnet} are a class of product-form queueing networks in which both positive and negative customers are allowed; the first ones behave as we are used to in traditional queueing networks whether the negative ones at arrival to a station delete a positive customer, if any is present, or vanishes otherwise. In \cite{gnet2} it was shown that these negative customers may act as triggers, meaning that they can move a customer from a non-empty queue to another one. The class of G-networks has been then further investigated to comprehend chains of instantaneous state changes that can be modelled as the propagation of instantaneous transition as shown in \cite{prop, prop2}. Accordingly, we write $$P=(a\rightarrow b, \top).Q$$ to denote a passive action with type $a$ that takes process $P$ to $Q$ and instantaneously synchronizes as active on type $b$. The rate at which the transition synchronizes on type $b$ is well-defined and equal to the reversed rate of the passive action with type $a$. As we shall see in the next chapter, this property will be extremely  useful for the modelling of the system we studied.

\paragraph*{Example.} We now give an example of an RCAT application, showing also that the result obtained through this alternative method is exactly the same if we were applying Jackson's theorem. The system we are going to analyse is the queueing network showed in fig. \ref{fig12} and its PEPA specification is as follows.
\begin{align*}
Q_{1,n} &= (a_{01},\lambda_1).Q_{1,n+1} \quad n\geq 0\\
Q_{1,n} &= (a_{13},\mu_1).Q_{1,n-1} \quad n >0\\
Q_{2,n} &= (a_{32},\top).Q_{2,n+1} \quad n\geq 0\\
Q_{2,n} &= (a_{02},\lambda_2).Q_{2,n+1} \quad n\geq 0\\
Q_{2,n} &= (a_{23},\mu_2).Q_{2,n-1} \quad n > 0\\
Q_{3,n} &= (a_{23},\top_{23}).Q_{3,n+1} \quad n\geq 0\\
Q_{3,n} &= (a_{13},\top_{13}).Q_{3,n+1} \quad n\geq 0\\
Q_{3,n} &= (a_{32},\mu_3p).Q_{3,n-1} \quad n > 0\\
Q_{3,n} &= (a_{30},\mu_2(1-p)).Q_{3,n-1} \quad n > 0\\
\end{align*}
with $(W_0\underset{a_{13}}\bowtie W_3) || (W_1\underset{a_{23}, a_{32}}\bowtie W_3)$.
\begin{figure}
	\centering
	\includegraphics[width=12cm]{Images/img12.png}
	\caption{Queueing Network under study.}
	\label{fig12}
\end{figure}

In order to verify that our model satisfies the three structural conditions of RCAT we draw part of the Markov chain underlying each queue, highlighting the different transitions between states (fig. \ref{fig13}).

\begin{figure}
	\centering
	\includegraphics[width=12cm]{Images/img13.png}
	\caption{Graphical representation of the PEPA model of the system showed in fig. \ref{fig12}}
	\label{fig13}
\end{figure}

At this point we can state that the first two structural conditions are satisfied as all passive actions are enabled in every state and the reversed active actions are also enabled. In fact, by inspection of the image above we can see there is an outgoing arc for every state for every passive action and an incoming  arc for every state for every reversed active action. To verify the third  condition we need to retrieve traffic conditions in order to check that the rate of each reversed active  action is constant. Considering the  $x_i$ with the same subscripts as action labels  as the rates  of  those actions, we have the following traffic equations:

\begin{equation*}
\begin{cases}
x_{13}=\lambda_1\\
x_{23}=\lambda_2 + x_{32}\\
x_{32}=(x_{13}+x_{23})\dfrac{\mu_3p}{\mu_3p+\mu_3(1-p)}
\end{cases}
\qquad
\rightarrow
\qquad
\begin{cases}
x_{13}=\lambda_1\\
x_{23}=\lambda_2 + x_{32}\\
x_{32}=(x_{13}+x_{23})p
\end{cases}
\end{equation*}

these will  give the following results: $$x_{13}=\lambda_1,\quad x_{23}=\dfrac{p\lambda_1+\lambda_2}{(1-p)},\quad x_{32}=\dfrac{\lambda_1+\lambda_2}{(1-p)}$$

Since the reversed rates  have been found and they are constant, RCAT tells us not only a product form exists but also which one it is; as a matter of fact RCAT states that the product form is given by the stationary distribution at the queues where all the $\top_i$ have been replaced by the correspondent reversed rates. In fact, we have:
\begin{align*}
\pi_1(n_1) &= \bigg(\dfrac{\lambda_1}{\mu_1}\bigg)^{n_1}\\\\
\pi_2(n_2) &= \bigg(\dfrac{p\lambda_1+\lambda_2}{(1-p)\mu_2}\bigg)^{n_2}\\\\
\pi_3(n_3) &= \bigg(\dfrac{\lambda_1+\lambda_2}{(1-p)\mu_3}\bigg)^{n_3}
\end{align*}

Now, to check our result is true we retrieve the product form using Jackson's theorem, meaning  again traffic equations, so we will have:

\begin{equation*}
\begin{cases}
e_{1}=\lambda_1\\
e_{2}=\lambda_2 + e_{3}p\\
e_{3}=e_{1}+e_{2}
\end{cases}
\end{equation*}

which results will be

$$e_{1}=\lambda_1,\quad e_{2}=\dfrac{p\lambda_1+\lambda_2}{(1-p)},\quad e_{3}=\dfrac{\lambda_1+\lambda_2}{(1-p)}$$. Given that by Jackson's  theorem we  would have 
$$\pmb{\pi}(n_1,n_2,n_3)=\bigg(\dfrac{e_1}{\mu_1}\bigg)^{n_1}\bigg(\dfrac{e_2}{\mu_2}\bigg)^{n_2}\bigg(\dfrac{e_3}{\mu_3}\bigg)^{n_3}$$
Substituting the just obtained results we will obtain the same result as before, meaning we did right.